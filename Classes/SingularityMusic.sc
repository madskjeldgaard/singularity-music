SingularityMusic{
    classvar <files;

    *initClass{
        StartUp.add({
            var pkgName = 'singularity-music';
            var path = PathName(Main.packages.asDict.at(pkgName)) +/+ "finished-scenes";
            files = IdentityDictionary.new;

            path.filesDo{|file|
                files.put(file.fileNameWithoutExtension.asSymbol, file.fullPath)
            };

        })
    }

    *get{|scenename, totalDuration, fadeIn, fadeOut, gain|
        var file = files.at(scenename.asSymbol);

        ^if(file.notNil, {
            file.load().value(totalDuration, fadeOut, fadeIn, gain);
        }, {
            "% does not exist. Has to be one of: %".format(scenename, files.keys).error;
            nil
        })
    }
}
