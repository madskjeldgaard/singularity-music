// Setup
(
s.boot;
Komet.logLevel(\debug);
Komet.start(numChannelsOut:2, build:true);
s.plotTree;
)
(
var pat;
var sourcePattern = Pbind(
    \type, \k,

    \base, \complex,
    \env, \adsr,
    \filter, \none,
    \category, \synthetic,

    \freq, Pwhite(100, 520),
    \freqOffset, 0.0,

    \dur, 8.0,
    \lagTime, 0.0,
    \legato, 2,

    \amp, 0.125,

    \modFreq, Pwhite(99.0,100.0),
    \amMod, Pwhite(0.0,0.25),
    \fmMod, 0.2,
    \timbreMod, Pwhite(0.0,0.25),

    \modclip, 0.0,
    \modskew, 0.0,
    \modphase, 0.25,
    \mainclip, 0.2,
    \mainskew, 0.3,
    \mainphase, 1.25,

    \pan, Pwhite(-0.5,0.5),
    \panFreq, Pwhite(0.001,0.2),
    \autopan, 1.0,
    \panShape, KPanShape.noise,

    \gate, 1.0,
    \atk, 0.5,
    \sus, 1,
    \rel, 0.5,
    \curve, Pwhite(-5,5),

);

// Filter
pat = POutFX(
    sourcePattern: sourcePattern,
    fxDefName: KometSynthLib.at(\fx,  \channelized, \vadim, \synthDefName),
    fxArgPairs: [
        \dur, 16,
        \spread, 0.125,
        \cutoff, Pwhite(100,250),
        \res, Pwhite(0.5,0.75),
        \lagTime, Pkey(\dur) / 2,
    ],
    releaseTime: 10
);

Pdef(\singularity_complexdrone1, pat).play;

)
